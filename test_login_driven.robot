

*** Settings ***
Library           Selenium2Library
Library           chromedriver     ${EXECDIR}/chromedriver.exe

Suite Setup       Open Browser To Login Page
Test Template     Login
*** Variables ***
${HOMEPAGE}    https://ct.sit.orangetheoryfitness.net/#/login
${BROWSER}     chrome
${DELAY}       1
${USERNAME}    sit 4
${PASSWORD}    Or@nge201
${STUDIO_SUGGESTION_DELAY}    2

*** Test Cases ***
Enter Username          ${USERNAME}

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${HOMEPAGE}    ${BROWSER}
    Set Selenium Speed    ${DELAY}
    Select Language
    Input Studio Name      ${USERNAME}
    Set Selenium Speed    ${STUDIO_SUGGESTION_DELAY}
    Select Studio
    Input Password      ${PASSWORD}
    Submit Credentials

Select Language
    Click Element    xpath=//*[@id="root"]/div/div[1]/div[1]/div/div[2]/div/div/div/div/span[2]
    Click Element    xpath=//*[@id="react-select-2--option-1"]

Enter Credentials
    [Arguments]      ${username}    ${password}
    Input Username ${username}
    Input Password ${password}

Input Studio Name
    [Arguments]    ${username}
    Input Text     xpath=//*[@id="studioUsername"]    ${username}

Select Studio
    Click Element    xpath=//*[@id="suggestionList"]/li/span

Input Password
    [Arguments]    ${password}
    Input Text     xpath=//*[@id="studioPassword"]    ${password}

Submit Credentials
    Click Element    xpath=//*[@id="root"]/div/div[1]/div[1]/form/div[3]/button